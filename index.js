const express = require('express');
const app = express();
const router = require('./routes/main');

app.use('/', router);

module.exports.helloWorld = app;
