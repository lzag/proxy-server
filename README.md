# Proxy server Node.js

Node.js server that makes calls to the api


Requirements: Google Cloud SDK

Installation:
The easiest way to install GCloud SDK is using the snap store:
``` bash
sudo apt install google-cloud-sdk

``` bash
npm install @google-cloud/functions-framework
```

A script in the package json needs to be added to start the dev server with the name of the entry point function. 
Please keep in mind that the doesn't reflect exactly the production environment.

``` json
"start": "functions-framework --target=<FUNCTION_NAME"
```

More information on working with GCloud SDK:
[GCloud Documentation](https://cloud.google.com/sdk/docs/initializing "GCloud Docs")
