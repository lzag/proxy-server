const express = require('express');
const router = express.Router();
const mainController = require('../controllers/main');
const cors = require('../controllers/cors');

router.route('/')
  .get(
    cors,
    mainController
    );

module.exports = router;
